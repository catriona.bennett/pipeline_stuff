# CHANGELOG

All notable changes to this project will be documented in this file. Date should be written in the format of `(YYYY-MM-DD)`

### New

### Changes

### Fixes

### Breaks

## 0.0.3 - (2021-09-08)

### Fixes
* Fixes bug and improves pipeline

## 0.0.2 - (2021-09-06)

### Changes
* Change pipeline implementation

## 0.0.1 - (2021-08-16)

### New
* Add pipeline
