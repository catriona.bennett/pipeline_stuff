#!/bin/bash

echo_error() {
  RED="\033[31m\033[1m"
  RESET="\033[0m"
  echo -e "${RED}$*${RESET}" >&2
}
